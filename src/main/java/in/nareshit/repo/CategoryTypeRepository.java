package in.nareshit.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.nareshit.entity.CategoryType;

public interface CategoryTypeRepository extends JpaRepository<CategoryType, Long> {
	
	@Query("SELECT id, name From CategoryType")
	List<Object[]> getCategoryTypeIdAndName();

}
