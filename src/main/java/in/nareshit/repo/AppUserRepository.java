package in.nareshit.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nareshit.entity.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

}
