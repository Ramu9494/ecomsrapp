package in.nareshit.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nareshit.entity.Coupon;

public interface CouponRepository extends JpaRepository<Coupon, Long> {

}
