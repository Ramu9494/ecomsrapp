package in.nareshit.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nareshit.entity.Shipping;

public interface ShippingRepository extends JpaRepository<Shipping, Long> {

}
