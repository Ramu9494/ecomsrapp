package in.nareshit.service;

import java.util.List;

import in.nareshit.entity.Coupon;

public interface ICouponService {
	
	Long saveCoupon(Coupon coupon);
	void updateCoupon(Coupon coupon);
	void deleteCoupon(Long id);
	Coupon getOneCoupon(Long id);
	List<Coupon> getAllCoupons();


}
