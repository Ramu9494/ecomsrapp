package in.nareshit.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import in.nareshit.constants.UserStatus;
import in.nareshit.entity.User;
import in.nareshit.repo.UserRepository;
import in.nareshit.service.IUserService;
import in.nareshit.util.AppUtil;

@Service
public class UserServiceImpl implements IUserService, UserDetailsService {
	
	@Autowired
	private UserRepository repo;
	
	@Autowired
	private BCryptPasswordEncoder encoder;

	@Override
	public Long saveUser(User user) {
		/// Generating passwrd
		String pwd = AppUtil.genPwd();
		System.out.println(user.getEmail()+"-"+pwd+"-"+user.getRole().name());
		
		//read generated password and encode
		String encPwd = encoder.encode(pwd);
		
		//set back to user object
		user.setPassword(encPwd);
		
		user.setStatus(UserStatus.INACTIVE.name());
		
		// TODO : SENDIG EMAIL
		
		return repo.save(user).getId();
		

	}

	@Override
	public Optional<User> findByEmail(String email) {		
		return repo.findByEmail(email);
	}

	@Override
	public List<User> getAllUsers() {	
		return repo.findAll();
	}

	@Override
	public UserDetails loadUserByUsername(String username) 
			throws UsernameNotFoundException {
		// load user by username(email)
		Optional<User> opt = findByEmail(username);
		if(!opt.isPresent()) {
			throw new UsernameNotFoundException("Not exist");
		}else {
			//read user object
			User user = opt.get();
			return new org.springframework.security.core.userdetails
					//username,password,List<GA>(RoleAsString)
					.User(
							user.getEmail(),
							user.getPassword(),
							Arrays.asList(
											new SimpleGrantedAuthority(
													user.getRole().name()
													)
											)
							);
		}
		
	}

}
