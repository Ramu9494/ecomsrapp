package in.nareshit.service;

import java.util.List;
import java.util.Optional;

import in.nareshit.entity.User;

public interface IUserService {
	
	Long saveUser(User user);
	Optional<User> findByEmail(String email);
	List<User> getAllUsers();


}
