package in.nareshit.service;

import java.util.List;

import in.nareshit.entity.AppUser;

public interface IAppUserService {
	
	Long saveAppUser(AppUser user);
	List<AppUser> getAllAppUser();

}
